package com.example.cloud_microservice_user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CloudMicroserviceUserApplication {

    public static void main(String[] args) {
        SpringApplication.run(CloudMicroserviceUserApplication.class, args);
    }

}
